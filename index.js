function createNewUser () {
    let firstName = prompt("Input your name");
    let lastName = prompt("Input your last name");
    let birthday = prompt("Input your birthday - dd.mm.year");
    let newUser = {
        firstname: firstName,
        lastname: lastName,
        bday: birthday,
        getLogin (){
            let username = this.firstname.charAt(0) + this.lastname ;
            return username.toLowerCase()
        },
        getAge (){
            let birthYear = this.bday.slice(6);
            return ( new Date().getFullYear()-birthYear);
        },
        getPassword() {
            return this.firstname.charAt(0).toUpperCase() + this.lastname.toLowerCase() + this.bday.slice(6);

        }
    }
    console.log(newUser);
    return newUser;
}

const newUser = createNewUser() ;
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());